﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_1.Models
{
    public class Account
    {
        [Key]
        public int AccountID { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public String Password { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.EmailAddress,ErrorMessage="Địa chỉ email không hợp lệ")]
        public String Email { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [MaxLength(100,ErrorMessage="Tối đa {1} kí tự")]
        [Display(Name = "Tên")]
        public String FirstName { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [MaxLength(100, ErrorMessage = "Tối đa {1} kí tự")]
        [Display(Name = "Họ")]
        public String LastName { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}