﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog2_1.Models
{
    public class BlogDbContext:DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
        public DbSet<Tag> Tags { set; get; }
        public DbSet<Account> Accounts { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(p => p.Tags).WithMany(t => t.Posts).Map(p => p.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Post_Tag"));
           

        }
        
    }
}