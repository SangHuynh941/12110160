﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_1.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage="Bắt buộc")]
        [MinLength(50,ErrorMessage="Nội dung quá ngắn. Tối thiểu {1} kí tự")]
        [Display(Name = "Nội dung")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.DateTime,  ErrorMessage = "Ngày tháng không hợp lệ")]
        [Display(Name = "Ngày tạo")]
        [DisplayFormat(DataFormatString="dd-mm-yyyy",ApplyFormatInEditMode=true)]
        public DateTime DayCreated { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày tháng không hợp lệ")]
        [Display(Name = "Ngày cập nhật")]
        [DisplayFormat(DataFormatString = "dd-mm-yyyy", ApplyFormatInEditMode = true)]
        public DateTime DayUpdated { set; get; }


        [Required(ErrorMessage = "Bắt buộc")]
        public String Author { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}