﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_1.Models
{
    public class Post
    {

        public int ID { set; get; }
        [Required(ErrorMessage="Bắt buộc")]
        [StringLength(maximumLength:500,ErrorMessage="Được nhập từ {2}-{1} kí tự",MinimumLength=20)]
        [Display(Name = "Tiêu đề")]
        public String Title { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.Text)]
        [MinLength(50, ErrorMessage = "Nội dung quá ngắn. Tối thiểu {1} kí tự")]
        [Display(Name="Nội dung")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.DateTime, ErrorMessage ="Ngày tháng không hợp lệ")]
        [Display(Name = "Ngày tạo")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DayCreated { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.DateTime,  ErrorMessage = "Ngày tháng không hợp lệ")]
        [Display(Name = "Ngày cập nhật")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DayUpdated { set; get; }

        public int AccountID { set; get; }
        public virtual Account Account { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }



    }
}