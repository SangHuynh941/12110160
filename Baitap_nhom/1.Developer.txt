Lifetime _ Save your time. Save your life.

Trang web được tạo nên từ ý tưởng giúp mọi người sắp xếp mọi sự kiện trong cuộc sống nhằm tận dụng thời gian tốt nhất để thực hiện mọi việc mà bạn cần, bạn muốn làm; giúp bạn thực hiện được mục tiêu mà bạn đặt ra trong cuộc sống cũng như dành thời gian cho những người xung quanh: gia đình, học tập, công việc, bạn bè, người yêu….
Mô tả chức năng: 
- Mỗi bạn sẽ được tạo cho mình một tài khoản riêng, với tên người dùng và một mật khẩu riêng để bảo mật. Sau khi tạo tài khoản người dùng sẽ cung cấp một số thông tin cần thiết như: ngày sinh, giới tính, email, số điện thoại, sở thích…và đặt biệt là mục tiêu cuộc sống của bạn nhằm giúp bạn có thể sử dụng hiệu quả những chức năng của trang web sau này.
- Sau khi có một tài khoản trên trang, bạn đưa ra mục tiêu cho cuộc sống của bản thân mình và tự tạo thời gian biểu cho bản thân để hoàn thành mục tiêu đó, chia nhỏ theo từng giai đoạn theo sự sắp xếp của bản thân sao cho hợp lý nhất:
	+ Theo từng năm: là những mục tiêu được chia nhỏ theo năm, sau mỗi năm, chúng tôi sẽ giúp bạn đánh giá % bạn thực hiện được mục tiêu.
	+ Theo từng tháng: là những công được chia nhỏ ra từ mục tiêu của năm đó, sự phân bố công việc được phân phù hợp cho từng tháng, chúng tôi sẽ giúp bạn có những gợi ý giúp ích cho bạn.
	+ Theo từng tuần: tiếp tục chia nhỏ công việc của mỗi tháng.
	+ Theo từng ngày: đây là mục quan trọng nhất, ngoài để thực hiện các mục tiên bạn đặt ra trước đó, bạn còn phải sắp xếp thời gian cho những khoản thiết yếu của cuộc sống, chúng tôi sẽ giúp bạn.
- Bạn sẽ không hề mắc kẹt bởi những công việc bạn đặt ra để thực hiện, chúng tôi hỗ trợ kết nối đến facebook và google giúp bạn chia sẻ kế hoạch của mình đến bạn bè, từ đó nhận được hỗ trợ, góp ý từ bạn bè và luôn  nhắc nhở để đảm bảo bạn không bỏ lỡ bất cứ công việc nào. Đồng thời, bạn cũng đóng góp cho mọi người về bản kế hoạch của họ, từ đó rút ra được những thông tin bổ ịch cho bản thân.
- Ngoài ra, từ sở thích của bạn, chúng tôi cung cấp thông tin cho bạn những hoạt động, sự kiện phù hợp với sở thích, đồng thời bạn được trao đổi với bạn bè cùng sở thích về những hoạt động, sự kiện cùng tham gia với nhau. Đồng thời, chính bạn cũng có thể đề xuất tổ chức sự kiện cho chính mình cũng như bạn bè cũng sở thích để đảm bảo bạn luôn bận rộn trong những hoạt động có ích.
