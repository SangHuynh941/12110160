﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog5_1.Models;

namespace Blog5_1.Controllers
{
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View(db.Post.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Post.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post,string Content)
        {
            if (ModelState.IsValid)
            {
                post.DayCreated = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileId = userid;

                List<Tag> Tags = new List<Tag>();
                string[] TagContent = Content.Split(',');
                foreach(string item in TagContent)
                {
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if(ListTag.Count()>0)
                    {
                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);

                    }
                    Tags.Add(tagExits);
                }
                post.Tags = Tags;
                

                db.Post.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Post.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Post.Find(id);
            db.Post.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}