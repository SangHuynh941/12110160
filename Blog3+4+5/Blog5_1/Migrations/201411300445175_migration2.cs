namespace Blog5_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Post_Tag",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
            AddColumn("dbo.Posts", "Tag_ID", c => c.Int());
            AddForeignKey("dbo.Posts", "Tag_ID", "dbo.Tags", "ID");
            CreateIndex("dbo.Posts", "Tag_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post_Tag", new[] { "TagID" });
            DropIndex("dbo.Post_Tag", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "Tag_ID" });
            DropForeignKey("dbo.Post_Tag", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Post_Tag", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "Tag_ID", "dbo.Tags");
            DropColumn("dbo.Posts", "Tag_ID");
            DropTable("dbo.Post_Tag");
            DropTable("dbo.Tags");
        }
    }
}
