﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog5_1.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Bắt buộc")]
        [StringLength(maximumLength: 100, ErrorMessage = "Được nhập từ 10-100 kí tự", MinimumLength = 10)]
        [Display(Name = "Nội dung")]
        public String Content { set; get; }

        public virtual ICollection<Post> Post { set; get; }

        public ICollection<Models.Post> Posts { get; set; }
    }
}