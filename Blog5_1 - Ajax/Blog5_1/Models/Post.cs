﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog5_1.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [StringLength(maximumLength: 500, ErrorMessage = "Được nhập từ 20-500 kí tự", MinimumLength = 20)]
        [Display(Name = "Tiêu đề")]
        public String Title { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.Text)]
        [MinLength(50, ErrorMessage = "Nội dung quá ngắn. Tối thiểu 50 kí tự")]
        [Display(Name = "Nội dung")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày tháng không hợp lệ")]
        [Display(Name = "Ngày tạo")]
        public DateTime DayCreated { set; get; }

       

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileId { set; get; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}