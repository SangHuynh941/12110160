﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog5_1.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [MinLength(50, ErrorMessage = "Nội dung quá ngắn. Tối thiểu 50 kí tự")]
        [Display(Name = "Nội dung")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Bắt buộc")]
        [DataType(DataType.DateTime, ErrorMessage = "Ngày tháng không hợp lệ")]
        [Display(Name = "Ngày tạo")]
        public DateTime DayCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DayCreated).Minutes;
            }
        }

        public virtual Post Post { get; set; }
        public int PostID { get; set; }
    }
}