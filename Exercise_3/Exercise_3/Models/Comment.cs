﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exercise_3.Models
{
    public class Comment
    {
        [Key]
        public int ID { get; set; }
        public int Post_ID { get; set; }
        public String Body { get; set; }
    }
}