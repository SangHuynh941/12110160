﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Exercise_3.Models
{
    public class Post
    {
        [Key]
        public int ID { set; get; }
        public String Title { set; get;}
        public String Body { set; get; }
    }
    public class BlogDbContext : DbContext
    {
        public DbSet<Post> Post { get; set; }

        public DbSet<Comment> Comments { get; set; }

    }
}