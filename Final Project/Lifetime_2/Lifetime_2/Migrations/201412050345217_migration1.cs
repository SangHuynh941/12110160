namespace Lifetime_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        TaskID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Deadline = c.DateTime(nullable: false),
                        Priority = c.String(),
                        Description = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Done = c.Boolean(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TaskID)
                .ForeignKey("dbo.UserProfile", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Task_TaskID = c.Int(),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Tasks", t => t.Task_TaskID)
                .Index(t => t.Task_TaskID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "Task_TaskID" });
            DropIndex("dbo.Tasks", new[] { "UserID" });
            DropForeignKey("dbo.Comments", "Task_TaskID", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "UserID", "dbo.UserProfile");
            DropTable("dbo.Comments");
            DropTable("dbo.Tasks");
            DropTable("dbo.UserProfile");
        }
    }
}
