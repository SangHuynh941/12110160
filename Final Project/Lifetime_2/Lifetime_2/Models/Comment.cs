﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lifetime_2.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public String Body { get; set; }
        public DateTime DateCreated { get; set; }


        public virtual Task Task { get; set; }
        public int TaskID {get; set;}

        public virtual Status Status { get; set; }
        public int SatusID { get; set;}
    }
}