﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lifetime_2.Models
{
    public class Event
    {
        public int EventID { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Reply> Replys { get; set; }
        

    }
}