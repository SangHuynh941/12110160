﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lifetime_2.Models
{
    public class Rate
    {
        public int UserProfileID { get; set; }
        public int TaskID { get; set; }
        public int Rates { get; set; }

        public virtual UserProfile UserProfile {get;set;}
        public virtual Task Task { get; set; }
    }
}