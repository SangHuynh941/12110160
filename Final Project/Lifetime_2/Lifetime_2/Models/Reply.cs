﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lifetime_2.Models
{
    public class Reply
    {
        public int UserProfileID { get; set; }
        public int EventID { get; set; }
        public String Content { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public virtual Event EventID { get; set; }
    }
}