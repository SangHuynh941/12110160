﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Lifetime_2.Models
{
    public class Task
    {
        public int TaskID { get; set; }

        [Required]
        public String Title { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Deadline { get; set; }

        [Required]
        public String Priority { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public String Description { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public Boolean Done { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public int UserID { get; set; }
        


        public virtual ICollection<Comment> Comments { get; set; }
       
        
    }
}